# thrvld

## description

A risc-v64 linker with features being refined.

## build

### Arch Linux

Install dependencies

```bash
sudo pacman -S base-devel
sudo pacman -S riscv64-elf-binutils riscv64-elf-gcc riscv64-elf-gdb riscv64-elf-newlib
```

build

```bash
# build, make sure u have rust toolchains
cargo build
# clean
cargo clean
```

## usage

```bash
# entry project dir
./target/debug/thrvld [<file.o>]
```

## todo

- [x] read ELF Header and Section Header Table

- [x] parse ELF Header and other Headers

- [ ] parse cli args

- [ ] parse static library

- [ ] parse undefined symbols and remove unused files

- [ ] processing Mergeable Sections

- [ ] processing Output Sections

- [ ] processing Phdr and Merged Sections

- [ ] processing redirections

## maintainer

TenHian : tenhian.cn@gmail.com
