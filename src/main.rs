mod elf;
mod obj;

use std::{env, io};
use std::fs::File;
use std::io::ErrorKind;
use std::process;
use crate::elf::{Elf, elf_get_name, is_elf};
use crate::obj::Obj;


fn main() -> Result<(), io::Error> {
    let path = parse_cli();
    let mut file = file_test(path)?;

    if !is_elf(&mut file).expect("An unexpected error occurred while determining the elf file.") {
        return Err(io::Error::new(ErrorKind::InvalidInput, "Not an elf file!".to_string()));
    }

    let mut elf = Elf::new(file.try_clone().unwrap())?;
    dbg!(&elf.ehdr);
    dbg!(&elf.shdrs);

    elf.get_shshrtab()?;
    dbg!(&elf.shstrtab);

    for i in elf.shdrs.iter() {
        dbg!(elf_get_name(&elf.shstrtab, i.name));
    }

    let mut obj = Obj::new(file)?;
    obj.parse()?;
    for i in obj.elf.syms.iter(){
        dbg!(elf_get_name(&obj.elf.symstrtab,i.name));
    }

    Ok(())
}

fn parse_cli() -> String {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Missing required parameters.\n\
        Try thrvld --help");
        process::exit(1);
    }
    args[1].clone()
}

fn file_test(path: String) -> Result<File, io::Error> {
    let f = File::open(path)?;
    Ok(f)
}