use std::fs::File;
use std::{io, slice};
use std::mem;
use crate::elf::{Elf, Shdr, Sym};

pub struct Obj {
    pub elf: Elf,
    pub symtabsec: Shdr,
}

impl Obj {
    pub fn new(f: File) -> Result<Obj, io::Error> {
        let elf = Elf::new(f)?;
        Ok(Obj { elf , symtabsec: Default::default() })
    }

    pub fn parse(&mut self)->Result<(), io::Error>{
        self.symtabsec = self.elf.find_section(2).unwrap();
        let first_global = self.symtabsec.info;
        self.elf.first_global = first_global;
        self.fillup_elf_syms().unwrap();
        self.elf.symstrtab = self.elf.get_bytes_from_index(self.symtabsec.link as usize)?;

        Ok(())
    }

    pub fn fillup_elf_syms(&mut self)->Result<(), io::Error>{
        let buffer = self.elf.get_bytes_from_shdr(&self.symtabsec)?;
        let count  = buffer.len() / mem::size_of::<Sym>();
        let sym_slice = unsafe {
            let sym_ptr = buffer.as_ptr() as *const Sym;
            slice::from_raw_parts(sym_ptr, count)
        };
        self.elf.syms = sym_slice.to_vec();

        // dbg!(&self.elf.symtab);
        // dbg!(&self.elf.symtab.len());

        Ok(())
    }
}