use std::fs::File;
use std::io::{ErrorKind, Read, Seek, SeekFrom};
use std::{io, mem};

#[derive(Debug, Default)]
#[repr(C)]
pub struct Ehdr {
    pub ident: [u8; 16],
    pub type_: u16,
    pub machine: u16,
    pub version: u32,
    pub entry: u64,
    pub ph_off: u64,
    pub sh_off: u64,
    pub flags: u32,
    pub eh_size: u16,
    pub ph_ent_size: u16,
    pub rh_num: u16,
    pub sh_ent_size: u16,
    pub sh_num: u16,
    pub sh_strndx: u16,
}

#[derive(Debug, Default, Clone)]
#[repr(C)]
pub struct Shdr {
    pub name: u32,
    pub type_: u32,
    pub flags: u64,
    pub addr: u64,
    pub offset: u64,
    pub size: u64,
    pub link: u32,
    pub info: u32,
    pub addr_align: u64,
    pub ent_size: u64,
}

#[derive(Debug)]
#[repr(C)]
pub struct Elf {
    pub file: File,
    pub ehdr: Ehdr,
    pub shdrs: Vec<Shdr>,
    pub syms: Vec<Sym>,
    pub shstrtab: Vec<u8>,
    pub symstrtab: Vec<u8>,

    pub first_global:u32,
}

#[derive(Debug, Clone)]
#[repr(C)]
pub struct Sym {
    pub name: u32,
    pub info: u8,
    pub other: u8,
    pub shndx: u16,
    pub val: u64,
    pub size: u64,
}

impl Elf {
    pub fn new(mut f: File) -> Result<Elf, io::Error> {
        let ehdr_ = read_ehdr(&mut f)?;
        let shdr_ = read_shdr(&mut f, ehdr_.sh_off, ehdr_.sh_num)?;
        Ok(Elf {
            file: f,
            ehdr: ehdr_,
            shdrs: shdr_,
            syms: vec![],
            shstrtab: vec![],
            symstrtab: vec![],

            first_global: 0,
        })
    }

    pub fn get_shshrtab(&mut self) -> Result<(), io::Error> {
        let shshrndx =
            if !matches!(self.ehdr.sh_strndx, 0xffff) {
                self.ehdr.sh_strndx as u32
            } else { self.shdrs[0].link };

        // let s = &self.shdr[shshrndx as usize];
        // self.file.seek(SeekFrom::Start(s.offset))?;
        // let mut buffer = vec![0u8; s.size as usize];
        // self.file.read_exact(&mut buffer)?;
        // self.file.rewind()?;

        // let s = self.shdr[shshrndx as usize].clone();
        // let buffer = self.get_bytes_from_shdr(&s)?;

        let buffer = self.get_bytes_from_index(shshrndx as usize)?;

        self.shstrtab = buffer;

        // let data = String::from_utf8_lossy(&buffer);
        // let trimmed_data = data.trim_end_matches(char::from(0));
        // let string : String= trimmed_data.split(char::from(0)).map(String::from).collect();

        Ok(())
    }

    pub fn find_section(&self, ty: u32) -> Result<Shdr, io::Error> {
        for i in self.shdrs.iter() {
            if i.type_ == ty {
                return Ok(i.clone());
            }
        }
        Err(io::Error::new(ErrorKind::NotFound, "Not find any elf type matched!".to_string()))
    }

    pub fn get_bytes_from_shdr(&mut self, shdr: &Shdr) -> Result<Vec<u8>, io::Error> {
        let offset = shdr.offset;
        let size = shdr.size as usize;
        let mut eh_buffer = vec![0u8; size];
        self.file.seek(SeekFrom::Start(offset))?;
        self.file.read_exact(&mut eh_buffer)?;
        self.file.rewind()?;

        Ok(eh_buffer)
    }

    pub fn get_bytes_from_index(&mut self, index: usize) -> Result<Vec<u8>, io::Error> {
        self.get_bytes_from_shdr(&self.shdrs[index].clone())
    }
}

pub fn is_elf(file: &mut File) -> Result<bool, io::Error> {
    let mut prefix = [0_u8; 4];
    let k_elf: [u8; 4] = [0x7f_u8, 0x45_u8, 0x4c_u8, 0x46_u8];
    file.read_exact(&mut prefix)?;
    file.rewind()?;
    if k_elf == prefix {
        return Ok(true);
    }
    Ok(false)
}

pub fn read_ehdr(file: &mut File) -> Result<Ehdr, io::Error> {
    let mut buffer = [0u8; mem::size_of::<Ehdr>()];
    file.read_exact(&mut buffer)?;
    file.rewind()?;
    let ehdr: Ehdr = unsafe { mem::transmute(buffer) };
    Ok(ehdr)
}

pub fn read_shdr(file: &mut File, sh_off: u64, sh_num: u16) -> Result<Vec<Shdr>, io::Error> {
    let mut buffer = [0u8; mem::size_of::<Shdr>()];
    file.seek(SeekFrom::Start(sh_off))?;
    file.read_exact(&mut buffer)?;
    let shdr: Shdr = unsafe { mem::transmute(buffer) };

    let mut sh_num = sh_num as u64;
    if sh_num == 0 {
        sh_num = shdr.size;
    }
    let mut v_shdr = vec![shdr];
    for _ in 1..sh_num {
        file.read_exact(&mut buffer)?;
        let shdr: Shdr = unsafe { mem::transmute(buffer) };
        v_shdr.push(shdr);
    }
    file.rewind()?;

    Ok(v_shdr)
}

pub fn elf_get_name(shstrtab: &Vec<u8>, offset: u32) -> String {
    let slice = &shstrtab[offset as usize..];
    let zero_index = slice.iter().position(|&x| x == 0).unwrap_or(slice.len());
    let string_slice = &slice[..zero_index];
    String::from_utf8_lossy(string_slice).to_string()
}